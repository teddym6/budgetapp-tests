# Test Objective
### Add expense 
- add expense with valid data - check the entry in the list
- add expense with using (- minus for the amount used) - check the entry in the list
- add expense and use special characters for description - check the entry in the list
- add expense and use long description > 255 chars - > I should not be able to add
- add expense and check the total values ( inflow , outflow , balance at the bottom of the page)

### Add income 
- add income choosing the income Category - check the entry in the list 
- add income and check the total values ( inflow , outflow , balance at the bottom of the page)

### Update
- add expense and update description - changes should be reflected in the list and totals
- add expense and update amount - changes should be reflected in the list and totals 
- delete one expense - changes should reflected in the list and totals
- delete an income - changes should be reflected in the list and totals 
- cancel an update - no changes should be reflected

# Reports
- check reports - compare inflow with outflow 
- check reports spend per category 

# TBD
- max amount for in and out ?
- max nr of characters for description ?

# Output expectation
### One test its expected to fail , I'm expecting to not have the "Add" button enable when the in/out amount is 0.

# Prerequisites 

- chrome browser installed
- access to internet 
- access to the terminal

# How to run?

```
 Open the terminal and navigate to the folder where the project was checked out
  
  Linux / Mac OS  
 ./gradlew runSuite -Dcucumber.options='--tags @budgeting --plugin pretty'`

  For windows use gradlew.bat instead of gradlew 
````

### Run headless
```
If you want to run your browser headless, you can open your terminal session and export RUN_HEADLESS=anything
```

### Test report
``` 
A basic html report will be generated in build/reports/tests/runSuite/index.html which can be opened via browser
The location of the report will be outputed after each running session
```