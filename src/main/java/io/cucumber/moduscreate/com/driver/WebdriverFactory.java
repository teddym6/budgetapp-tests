package io.cucumber.moduscreate.com.driver;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.lang3.SystemUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class WebdriverFactory {

    private final static Logger logger = Logger.getLogger(WebdriverFactory.class);
    public static WebDriver driver;

    public static WebDriver createDesktop() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = browserOptions();
        driver = new ChromeDriver(chromeOptions);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(3, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(3, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return driver;
    }

    public static WebDriver createMobile() {
        WebDriverManager.chromedriver().setup();
        Map<String, String> mobileEmulation = new HashMap<>();
        mobileEmulation.put("deviceName", "Nexus 5");
        ChromeOptions chromeOptions = new ChromeOptions();
        if (SystemUtils.IS_OS_LINUX) {
            chromeOptions.addArguments("--headless");
        }
        chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
        logger.info("Running headless on mobile emulator");
        driver = new ChromeDriver(chromeOptions);
        return driver;
    }

    private static ChromeOptions browserOptions() {
        ChromeOptions chromeOptions = new ChromeOptions();

        if (SystemUtils.IS_OS_LINUX) {
            if (System.getenv("RUN_HEADLESS") != null) {
                logger.info("Running headless on Linux");
                chromeOptions.addArguments("--headless");
            }
            logger.info("Running with x11 on Linux");
        }
        if (SystemUtils.IS_OS_WINDOWS) {
            logger.info("Running headless on windows");
        }
        return chromeOptions;
    }

    @After
    /**
     * Embed a screenshot in test report if test is marked as failed
     */
    public void embedScreenshot(Scenario scenario) {
        try {
            if (scenario.isFailed()) {
                if (null != driver) {
                    try {
                        scenario.write("Current Page URL is " + driver.getCurrentUrl());
                        byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                        scenario.embed(screenshot, "image/png");
                    } catch (WebDriverException somePlatformsDontSupportScreenshots) {
                        System.err.println(somePlatformsDontSupportScreenshots.getMessage());
                    }
                }
            }
        } finally {
            if (null != driver) {
                driver.quit();
            }
        }
    }
}
