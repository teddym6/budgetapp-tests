package io.cucumber.moduscreate.com;

import org.openqa.selenium.By;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class BudgetPage extends BasePage {
    public BudgetPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(how = How.NAME, using = "value")
    private WebElement value;

    @FindBy(how = How.NAME, using = "description")
    private WebElement description;

    @FindBy(how = How.XPATH, using = "//button[contains(@class, 'submit')")
    private WebElement addButton;

    @FindBy(how = How.NAME, using = "categoryId")
    private WebElement categoryId;

    @FindBy(how = How.CSS, using = "tbody")
    private WebElement tableBody;

    // select a category
    public void selectCategory(String category) {
        waitForElementToBeVisible(categoryId, 3);
        Select dropdown = new Select(driver.findElement(By.name("categoryId")));
        dropdown.selectByIndex(Integer.parseInt(category));
    }

    public void fillDescription(String _description) {
        description.sendKeys(_description);
    }

    public void add() {
        description.submit();
    }

    public boolean addButtonIsEnable() {
        try {
            driver.findElement(By.cssSelector("button:disabled"));
            return false;
        } catch (NotFoundException e) {
            return true;
        }
    }

    public void clickOnLastEntry() {
        lastAmountEntered().click();
    }

    /*
    I get all the table rows from the expenses list

     */
    private List<WebElement> getTableRows() {
        List<WebElement> rows = tableBody.findElements(By.tagName("tr"));
        return rows;
    }


    /*
    Using the list of rows I can get the last one from the list
    And I can click on it to read te values I've entered in the flow

     */
    private WebElement lastAmountEntered() {
        int size = getTableRows().size();
        return getTableRows().get(size - 1);
    }


    public boolean isFiledValue(String expected){
        try {
            tableBody.findElement(By.xpath(String.format("//input[@value='%s']",expected)));
            return true;
        } catch (NotFoundException e) {
            return false;
        }
    }


    public void inputExpenseAmount(String amount) {
        value.sendKeys(amount);
    }
}
