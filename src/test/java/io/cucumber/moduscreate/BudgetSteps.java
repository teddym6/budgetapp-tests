package io.cucumber.moduscreate;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.cucumber.moduscreate.com.BudgetPage;
import io.cucumber.moduscreate.com.driver.WebdriverFactory;
import io.qameta.allure.AllureResultsWriter;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class BudgetSteps {

    WebDriver driver = WebdriverFactory.createDesktop();
    private BudgetPage budgetPage;


    public BudgetSteps() {
        budgetPage = new BudgetPage(driver);
    }

    @Given("Im on the budget page {string}")
    public void open_budget_app_page(String page) {
        driver.navigate().to("https://budget.modus.app/" + page);
    }

    @When("I select a category {string}")
    public void select_category(String category) {
        budgetPage.selectCategory(category);
    }


    @When("I input expense description as {string}")
    public void add_description(String description) {
        budgetPage.fillDescription(description);
    }

    @When("I input the expense amount {string}")
    public void add_expense_value(String value) {
        budgetPage.inputExpenseAmount(value);
    }

    @When("Add button should be {string}")
    public void check_add_button_state(String state) {
        if (state.equals("disabled")){
            Assert.assertFalse(budgetPage.addButtonIsEnable());
        } else {
            Assert.assertTrue(budgetPage.addButtonIsEnable());
        }
    }

    @When("I click on add button")
    public void submit() {
        budgetPage.add();
    }

    @When("I click on the last entry from the expense list")
    public void click_on_last_expense() {
        budgetPage.clickOnLastEntry();
    }

    @When("I can see added expense detail {string}")
    public void check_expense_description(String description) {
        Assert.assertTrue(budgetPage.isFiledValue(description));
    }

}
