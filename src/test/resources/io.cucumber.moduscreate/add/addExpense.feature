@budgeting
Feature: manage budget

  Scenario: As a user I should e able to add an expense with a valid amount and short description
    Given Im on the budget page "budget"
    When I select a category "5"
    And I input expense description as "nursery stuff"
    When I input the expense amount "1000"
    And I click on add button
    When I click on the last entry from the expense list
    Then I can see added expense detail "nursery stuff"
    Then I can see added expense detail "-1000"

  Scenario: As a user I can add an income and I can see it in the list
    Given Im on the budget page "budget"
    When I select a category "14"
    And I input expense description as "salary"
    When I input the expense amount "50000"
    And I click on add button
    When I click on the last entry from the expense list
    Then I can see added expense detail "salary"
    Then I can see added expense detail "50000"


  Scenario Outline: As a user I should not be able to input non numeric chars into value field, and 0 values
    Given Im on the budget page "budget"
    When I select a category "5"
    And I input expense description as "nursery stuff"
    When I input the expense amount "<value>"
    Then Add button should be "disabled"
    Examples:
      | value |
      | aaa   |
      | !@#@  |
      | ,     |
      # I think makes more sense to not have expenses which has 0 value , if is 0 , is not an expense , also not an income .
      # this test will fail
      | 0000  |